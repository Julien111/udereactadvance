import { useState, useEffect, Fragment}  from "react";

function Timer() {
  const [timer, setTimer] = useState(0);

  useEffect(() => {
    const interId = setInterval(() => {
      setTimer((timer) => timer + 1);
    }, 1000);

    return () => {
      alert("Composant détruit");
      clearInterval(interId);
    };
  }, []);

  return (
    <Fragment>
      <p className="timer">
        <b>{timer}</b>
      </p>
      <p className="timer">
        <b>{timer}</b>
      </p>
    </Fragment>
  );
}

export default Timer;
