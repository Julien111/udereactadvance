import {useState, useEffect} from "react";


export default function useDimension() {

    const [dimension, setDimension] = useState();

    useEffect(() => {
        window.addEventListener("resize", resizeDim);

        function resizeDim() {
          setDimension(window.innerWidth);
        }
        resizeDim();

        return () => {
          window.addEventListener("resize", resizeDim);
        };

    }, []);

    return dimension;
}

