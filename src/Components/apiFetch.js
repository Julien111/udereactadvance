//import { useState, useEffect } from "react";

//import "./App.css";

function App() {
  const [stateData, setStateData] = useState();

  useEffect(() => {
    fetch("https://api.thecatapi.com/v1/images/search").then(response => {
      return response.json();
    }).then(data => {
      //console.log(data);
      setStateData(data[0].url)
    });
  }, []);

  return (
    <div className="App">
      <h1 className="titre">Appel à API</h1>

      {stateData && <img src={stateData} alt="image de chat" style={{ width:"500px" }} />}
    </div>
  );
}

//export default App;
