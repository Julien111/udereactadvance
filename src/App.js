import { useState, useMemo, useCallback } from "react";
import Content from "./Components/Content";
import Memory from "./Components/Memory";
import useDimension from "./Components/useDimension";

import "./App.css";

function App() {
  const [toggle, setToggle] = useState([1, 2, 3]);

  const funcToggle = () => {
    const newArr = [...toggle];
    newArr.push(4);
    setToggle(newArr);
  };

  const tab = useMemo(() => {
    return ["a", "b", "c", "d"];
  }, []);

  const foo = useCallback(() => {
    console.log("clic");
  }, []);

  // propre hook    

  const browser = useDimension();
  console.log(browser);
  

  return (
    <div className="App">
      <h1 className="titre">Appel à API</h1>
      <Memory arr={tab} foo={foo}/>
      <h2>{toggle}</h2>
      <button onClick={() => funcToggle()}>Change toggle</button>

      <Content>
        <h2>Contenu de chiltren 1</h2>
        <p>Test pour afficher l'enfant 1.</p>
      </Content>
      <Content>
        <h2>Contenu de chiltren 2</h2>
        <p>Test pour afficher l'enfant 2.</p>
      </Content>
      <Content>
        <h2>Contenu de chiltren 3</h2>
        <p>Test pour afficher l'enfant 3.</p>
      </Content>
    </div>
  );
}

export default App;
